<?php

namespace App\Libraries\Repositories;

class Customer extends Base
{
    public function model()
    {
        return \App\Models\Customer::class;
    }

    public function addMeasurementGroup(int $id, array $data){
        $data['customer_id'] = $id;

        return app()->make(MeasurementGroup::class)->add($data);
    }

    public function updateMeasurementGroup(int $id, int $measurementGroupId, array $data){
        return app()->make(MeasurementGroup::class)->update($measurementGroupId, $data);
    }

    public function deleteMeasurementGroup(int $id, int $measurementGroupId, array $data){
    }
}
