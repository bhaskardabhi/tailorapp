<?php

namespace App\Libraries\Repositories;

class User extends Base
{
    public function model()
    {
        return \App\Models\User::class;
    }

    public function register(array $data)
    {
        if (isset($data['password'])) {
            $data['password'] = bcrypt($data['password']);
        }

        $model = $this->makeModel()->fill($data);

        return $model->save() ? $model : null;
    }

    public function findByEmail(string $email){
        return $this->makeModel()->whereEmail($email)->first();
    }

    public function findByMobile(string $mobile){
        return $this->makeModel()->whereMobile($mobile)->first();
    }

    public function getShops(int $id)
    {
        return $this->makeModel()->findOrFail($id)->shops;
    }

    public function addShop(int $id, array $data)
    {
        $data['user_id'] = $id;

        return app()->make(Shop::class)->add($data);
    }

    public function findShop(int $id, int $shopId)
    {
        return app()->make(Shop::class)->find($shopId);
    }

    public function updateShop(int $id, int $shopId, array $data)
    {
        return app()->make(Shop::class)->update($shopId, $data);
    }

    public function deleteShop(int $id, int $shopId)
    {
        return app()->make(Shop::class)->delete($shopId);
    }

    public function getMeasurementTypes(int $id)
    {
        return $this->makeModel()->findOrFail($id)->measurementTypes;
    }

    public function addMeasurementType(int $id, array $data)
    {
        $data['user_id'] = $id;

        return app()->make(MeasurementType::class)->add($data);
    }

    public function updateMeasurementType(int $id, int $measurementTypeId, array $data)
    {
        return app()->make(MeasurementType::class)->update($measurementTypeId, $data);
    }

    public function deleteMeasurementType(int $id, int $measurementTypeId)
    {
        return app()->make(MeasurementType::class)->delete($measurementTypeId);
    }
}
