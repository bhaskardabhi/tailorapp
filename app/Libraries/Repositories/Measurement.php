<?php

namespace App\Libraries\Repositories;

use DB;

class Measurement extends Base
{
    public function model()
    {
        return \App\Models\Measurement::class;
    }

    public function add(array $data){
        return DB::transaction(function () use ($data){
            $measurement = parent::add(array_only($data,['customer_id','is_active']));

            $this->addValues($measurement->id, isset($data['values']) ? $data['values'] : []);
    
            return $measurement->load('values');
        });
    }

    public function update($id, array $data){
        return DB::transaction(function () use ($id, $data){
            $measurement = parent::update($id, array_only($data,['customer_id','is_active']));

            $this->updateValues($id, isset($data['values']) ? $data['values'] : []);
    
            return $measurement->load('values');
        });
    }

    public function addValues(int $id, $values){
        return DB::transaction(function () use ($id, $values){
            foreach($values as $index => $value){
                $this->addValue($id, $value);
            }
    
            return true;
        });
    }

    public function addValue(int $id, $value){
        $value['measurement_id'] = $id;

        return app()->make(MeasurementValue::class)->add($value);
    }

    public function updateValues(int $id, $values){
        return DB::transaction(function () use ($id, $values){
            foreach($values as $index => $value){
                $this->updateValue($id, $value);
            }
    
            return true;
        });
    }

    public function updateValue(int $id, $value){
        $value['measurement_id'] = $id;

        if(isset($value['id'])){
            return app()->make(MeasurementValue::class)->update($value['id'], $value);
        } else {
            return app()->make(MeasurementValue::class)->add($value);
        }
    }
}
