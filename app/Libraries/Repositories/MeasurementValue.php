<?php

namespace App\Libraries\Repositories;

class MeasurementValue extends Base
{
    public function model()
    {
        return \App\Models\MeasurementValue::class;
    }
}