<?php

namespace App\Libraries\Repositories;

abstract class Base
{
    abstract public function model();

    public function makeModel()
    {
        return app()->make($this->model());
    }

    public function add(array $data)
    {
        $model = $this->makeModel()->fill($data);

        return $model->save() ? $model : null;
    }

    public function update(int $id, array $data)
    {
        return $this->makeModel()->findOrFail($id)->fill($data)->save();
    }

    public function delete(int $id)
    {
        return $this->makeModel()->findOrFail($id)->delete();
    }

    public function find(int $id)
    {
        return $this->makeModel()->find($id);
    }

    public function paginate()
    {
        return $this->makeModel()->paginate();
    }
}
