<?php

namespace App\Libraries\Repositories;

class MeasurementType extends Base
{
    public function model()
    {
        return \App\Models\MeasurementType::class;
    }
}