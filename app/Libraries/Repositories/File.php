<?php

namespace App\Libraries\Repositories;

use Carbon\Carbon;
use Image;

class File extends Base
{
    public function model()
    {
        return \App\Models\File::class;
    }

    public function add(array $data)
    {
        $image = Image::make($data['content']);
        $file = md5(($data['name'] ?? Carbon::now()->format("Y-m-d H:i:s")) . microtime()) . "." . $this->getExtensionFromBase64($data['content']);
        $image = $image->save(public_path("files/" . $file));

        return parent::add([
            'name' => $data['name'] ?? null,
            'file' => $file,
        ]);
    }

    public function getExtensionFromBase64($content)
    {
        return str_replace("image/", "", explode(':', substr($content, 0, strpos($content, ';')))[1]);
    }
}
