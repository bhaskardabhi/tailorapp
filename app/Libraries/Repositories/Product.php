<?php

namespace App\Libraries\Repositories;

class Product extends Base
{
    public function model()
    {
        return \App\Models\Product::class;
    }

    public function add(array $data)
    {
        return \DB::transaction(function () use ($data) {
            $measurementTypes = $data['measurement_types'] ?? [];

            $model = parent::add(array_except($data, ['measurement_types']));

            if (!$model) {
                return $model;
            }

            $this->addMeasurementTypes($model->id, $measurementTypes);

            return $model;
        });
    }

    public function update(int $id,array $data)
    {
        return \DB::transaction(function () use ($data, $id) {
            $measurementTypes = $data['measurement_types'] ?? [];

            $result = parent::update($id, array_except($data, ['measurement_types']));

            if (!$result) {
                return $result;
            }

            $this->updateMeasurementTypes($id, $measurementTypes);

            return $result;
        });
    }

    public function addPhotos(int $id, array $photos)
    {
        $this->find($id)->photos()->attach($photos);

        return true;
    }

    public function deletePhoto(int $id, int $photoId)
    {
        $this->find($id)->photos()->detach($photoId);

        return true;
    }

    public function addMeasurementTypes(int $id, array $measurementTypes)
    {
        $this->find($id)->measurementTypes()->attach($measurementTypes);

        return true;
    }

    public function updateMeasurementTypes(int $id, array $measurementTypes){
        $this->find($id)->measurementTypes()->sync($measurementTypes);

        return true;
    }

    public function deleteMeasurementType(int $id, int $measurementTypeId)
    {
        $this->find($id)->measurementTypes()->detach($measurementTypeId);

        return true;
    }
}
