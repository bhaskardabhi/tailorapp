<?php

namespace App\Libraries\Repositories;

class Shop extends Base
{
    public function model()
    {
        return \App\Models\Shop::class;
    }

    public function getProducts(int $id)
    {
        return $this->makeModel()->findOrFail($id)->products;
    }

    public function addProduct(int $id, array $data)
    {
        $data['shop_id'] = $id;

        return app()->make(Product::class)->add($data);
    }

    public function findProduct(int $id, int $productId)
    {
        return app()->make(Product::class)->find($productId)->load('measurementTypes');
    }

    public function updateProduct(int $id, int $productId, array $data)
    {
        return app()->make(Product::class)->update($productId, $data);
    }

    public function deleteProduct(int $id, int $productId)
    {
        return app()->make(Product::class)->delete($productId);
    }

    public function getCustomers(int $id)
    {
        return $this->makeModel()->findOrFail($id)->customers;
    }

    public function addCustomer(int $id, array $data)
    {
        $data['shop_id'] = $id;

        return app()->make(Customer::class)->add($data);
    }

    public function updateCustomer(int $id, int $customerId, array $data)
    {
        return app()->make(Customer::class)->update($customerId, $data);
    }

    public function deleteCustomer(int $id, int $customerId)
    {
        return app()->make(Customer::class)->delete($customerId);
    }

    public function getMeasurementTypes(int $id)
    {
        return $this->makeModel()->findOrFail($id)->measurementTypes;
    }

    public function addMeasurementType(int $id, array $data)
    {
        $data['shop_id'] = $id;

        return app()->make(MeasurementType::class)->add($data);
    }

    public function updateMeasurementType(int $id, int $measurementTypeId, array $data)
    {
        return app()->make(MeasurementType::class)->update($measurementTypeId, $data);
    }

    public function deleteMeasurementType(int $id, int $measurementTypeId)
    {
        return app()->make(MeasurementType::class)->delete($measurementTypeId);
    }
}
