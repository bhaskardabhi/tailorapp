<?php

namespace App\Listeners;

use Illuminate\Auth\Events\Login;

class UserLoggedIn
{
    /**
     * Handle the event.
     *
     * @param  IlluminateAuthEventsLogin  $event
     * @return void
     */
    public function handle(Login $event)
    {
        if (!$event->user->api_token) {
            $event->user->api_token = str_random(60);
            $event->user->save();
        }
    }
}
