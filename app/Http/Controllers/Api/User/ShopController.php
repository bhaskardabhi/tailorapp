<?php

namespace App\Http\Controllers\Api\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Users\Shops\Create;
use App\Http\Requests\Api\Users\Shops\Update;
use App\Libraries\Repositories\User;
use Illuminate\Http\Request;

class ShopController extends Controller
{
    public function index(Request $request, $userId)
    {
        return app()->make(User::class)->getShops($userId);
    }

    public function store(Create $request, $userId)
    {
        return app()->make(User::class)->addShop(
            $userId,
            $request->only('name', 'address', 'mobile', 'email')
        );
    }

    public function show($userId, $shopId)
    {
        return app()->make(User::class)->findShop(
            $userId,
            $shopId
        );
    }

    public function update(Update $request, $userId, $shopId)
    {
        return [
            "success" => app()->make(User::class)->updateShop(
                $userId,
                $shopId,
                $request->only('name', 'address', 'mobile', 'email')
            ),
        ];
    }

    public function destroy(Request $request, $userId, $shopId)
    {
        return [
            "success" => app()->make(User::class)->deleteShop(
                $userId,
                $shopId
            ),
        ];
    }
}
