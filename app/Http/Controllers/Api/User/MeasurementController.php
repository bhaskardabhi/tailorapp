<?php

namespace App\Http\Controllers\Api\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Users\Measurements\Create;
use App\Http\Requests\Api\Users\Measurements\Update;
use App\Libraries\Repositories\User;
use Illuminate\Http\Request;

class MeasurementController extends Controller
{
    public function index(Request $request, $userId)
    {
        return app()->make(User::class)->getMeasurements($userId);
    }

    public function store(Create $request, $userId)
    {
        return app()->make(User::class)->addMeasurement(
            $userId,
            $request->only('name')
        );
    }

    public function update(Update $request, $userId, $MeasurementId)
    {
        return [
            "success" => app()->make(User::class)->updateMeasurement(
                $userId,
                $MeasurementId,
                $request->only('name')
            ),
        ];
    }

    public function destroy(Request $request, $userId, $MeasurementId)
    {
        return [
            "success" => app()->make(User::class)->deleteMeasurement(
                $userId,
                $MeasurementId
            ),
        ];
    }
}
