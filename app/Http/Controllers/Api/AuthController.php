<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Login;
use App\Http\Requests\Api\Register;
use App\Libraries\Repositories\User;
use Auth;

class AuthController extends Controller
{
    public function register(Register $request)
    {
        return app()->make(User::class)->register(
            $request->only('name', 'email', 'mobile', 'password')
        );
    }

    public function login(Login $request)
    {
        if (
            !Auth::attempt(['email' => $request->username, 'password' => $request->password])
            and
            !Auth::attempt(['mobile' => $request->username, 'password' => $request->password])
        ) {
            return response()->json(['message' => "Invalid Username Password"], 401);
        }

        return response()->json(Auth::user());
    }
}
