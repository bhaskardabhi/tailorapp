<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Libraries\Repositories\File;
use Illuminate\Http\Request;

class FileController extends Controller
{
    public function store(Request $request)
    {
        return app()->make(File::class)->add($request->only('name', 'content'));
    }

    public function show($id)
    {
        return app()->make(File::class)->find($id);
    }
}
