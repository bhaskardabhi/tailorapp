<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Libraries\Repositories\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function index(Request $request)
    {
        return app()->make(User::class)->paginate();
    }

    public function show($id)
    {
        return app()->make(User::class)->find($id);
    }
}
