<?php

namespace App\Http\Controllers\Api\Shop;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Users\Customers\Create;
use App\Http\Requests\Api\Users\Customers\Update;
use App\Libraries\Repositories\Shop;
use Illuminate\Http\Request;

class CustomerController extends Controller
{
    public function index(Request $request, $userId)
    {
        return app()->make(Shop::class)->getCustomers($userId);
    }

    public function store(Create $request, $userId)
    {
        return app()->make(Shop::class)->addCustomer(
            $userId,
            $request->only('name', 'address', 'mobile', 'email')
        );
    }

    public function update(Update $request, $userId, $customerId)
    {
        return [
            "success" => app()->make(Shop::class)->updateCustomer(
                $userId,
                $customerId,
                $request->only('name', 'address', 'mobile', 'email')
            ),
        ];
    }

    public function destroy(Request $request, $userId, $customerId)
    {
        return [
            "success" => app()->make(Shop::class)->deleteCustomer(
                $userId,
                $customerId
            ),
        ];
    }
}
