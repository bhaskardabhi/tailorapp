<?php

namespace App\Http\Controllers\Api\Shop;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Users\Shops\Products\Create;
use App\Http\Requests\Api\Users\Shops\Products\Update;
use App\Libraries\Repositories\Shop;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function index(Request $request, $shopId)
    {
        return app()->make(Shop::class)->getProducts($shopId);
    }

    public function store(Create $request, $shopId)
    {
        return app()->make(Shop::class)->addProduct(
            $shopId,
            $request->only('name', 'rate', 'measurement_types','note')
        );
    }

    public function show($shopId, $productId)
    {
        return app()->make(Shop::class)->findProduct(
            $shopId,
            $productId
        );
    }

    public function update(Update $request, $shopId, $productId)
    {
        return [
            "success" => app()->make(Shop::class)->updateProduct(
                $shopId,
                $productId,
                $request->only('name', 'rate', 'measurement_types','note')
            ),
        ];
    }

    public function destroy(Request $request, $shopId, $productId)
    {
        return [
            "success" => app()->make(Shop::class)->deleteProduct(
                $shopId,
                $productId
            ),
        ];
    }
}
