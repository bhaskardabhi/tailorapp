<?php

namespace App\Http\Controllers\Api\Shop;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Users\Shops\MeasurementTypes\Create;
use App\Http\Requests\Api\Users\Shops\MeasurementTypes\Update;
use App\Libraries\Repositories\Shop;
use Illuminate\Http\Request;


class MeasurementTypeController extends Controller
{
    public function index(Request $request, $shopID)
    {
        return app()->make(Shop::class)->getMeasurementTypes($shopID);
    }

    public function store(Create $request, $shopID)
    {
        return app()->make(Shop::class)->addMeasurementType(
            $shopID,
            $request->only('name')
        );
    }

    public function update(Update $request, $shopID, $customerId)
    {
        return [
            "success" => app()->make(Shop::class)->updateMeasurementType(
                $shopID,
                $customerId,
                $request->only('name')
            ),
        ];
    }

    public function destroy(Request $request, $userId, $customerId)
    {
        return [
            "success" => app()->make(Shop::class)->deleteMeasurementType(
                $userId,
                $customerId
            ),
        ];
    }
}
