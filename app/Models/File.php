<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class File extends Model
{
    protected $fillable = ['name', 'file'];
    protected $appends = ['url'];

    public function getUrlAttribute()
    {
        return url("files/" . $this->file);
    }
}
