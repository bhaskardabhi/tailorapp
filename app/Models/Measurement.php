<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Measurement extends Model
{
    protected $fillable = ['customer_id', 'is_active'];

    public function values(){
        return $this->hasMany(MeasurementValue::class);
    }
}
