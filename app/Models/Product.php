<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = ['name','note','rate', 'shop_id'];

    public function measurementTypes()
    {
        return $this->belongsToMany(MeasurementType::class);
    }
}
