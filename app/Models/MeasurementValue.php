<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MeasurementValue extends Model
{
    protected $fillable = ['measurement_id', 'measurement_type_id', 'value'];
}
