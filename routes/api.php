<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

// Shop
//     -> Customer
//     -> Product
//         -> Measurement-types
//     -> Measurement-types
//     -> Customer
//         -> Order
//             -> Measurement-types
//             -> Products
//             -> Customers
Route::group(["namespace" => "Api"], function () {
    Route::post('register', [
        'as' => 'register',
        'uses' => 'AuthController@register',
    ]);

    Route::post('login', [
        'as' => 'login',
        'uses' => 'AuthController@login',
    ]);

    
    Route::group(["namespace" => "User"], function () {
        Route::resource("users.shops", 'ShopController', ['only' => ['index', 'store', 'show', 'update', 'destroy']]);
    });

    Route::group(["namespace" => "Shop"], function () {
        Route::resource("shops.customers", 'CustomerController', ['only' => ['index', 'store', 'update', 'destroy']]);
        Route::resource("shops.measurement-types", 'MeasurementTypeController', ['only' => ['index', 'store', 'update', 'destroy']]);
        Route::resource("shops.products", 'ProductController', ['only' => ['index','show', 'store', 'update', 'destroy']]);
        Route::resource("shops.orders", 'OrderController', ['only' => ['index', 'store', 'update', 'destroy']]);
    });

    Route::group(["namespace" => "Order"], function () {
        Route::resource("orders.measurements", 'MeasurementController', ['only' => ['index', 'store', 'update', 'destroy']]);
        Route::resource("orders.products", 'MeasurementController', ['only' => ['index', 'store', 'update', 'destroy']]);
    });
});
