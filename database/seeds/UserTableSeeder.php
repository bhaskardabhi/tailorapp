<?php

use Illuminate\Database\Seeder;
use App\Libraries\Repositories\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = app()->make(User::class)->findByEmail("dabhibhaskar2@gmail.com");

        if(!$user){
            $user = app()->make(User::class)->register([
                'name' => 'Bhaskar Dabhi',
                'mobile' => '9145645945',
                'email' => 'dabhibhaskar2@gmail.com',
                'password' => '82028202',
            ]);
        }

        if(!$user){
            throw new Exception("Error while seeding user");
        }
    }
}
