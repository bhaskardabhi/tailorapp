<?php

use Illuminate\Database\Seeder;
use App\Libraries\Repositories\Measurement;
use App\Libraries\Repositories\User;
use App\Libraries\Repositories\Customer;

class CustomerMeasurementSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        return DB::transaction(function ()
        {
            $user = app()->make(User::class)->findByEmail("dabhibhaskar2@gmail.com");
            $customers = app()->make(User::class)->getCustomers($user->id);
            $measurements = app()->make(User::class)->getMeasurementTypes($user->id);
    
            foreach($customers as $index => $customer){
                $meansurement = [
                    'is_active' => true,
                    'values' => array_map(function ($measurement){
                        return [
                            'measurement_id' => $measurement['id'],
                            'value' => rand(10,100)
                        ];
                    },$measurements->toArray())
                ];

                app()->make(Customer::class)->addMeasurement($customer->id, $meansurement);
            }

            return true;
        });
    }
}
