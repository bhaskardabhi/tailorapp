<?php

use Illuminate\Database\Seeder;
use App\Libraries\Repositories\Measurement;
use App\Libraries\Repositories\User;

class MeasurementTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $measurementTypes = [
            ['name' => 'Neck'],
            ['name' => 'Length'],
            ['name' => 'Wrist'],
        ];

        $user = app()->make(User::class)->findByEmail("dabhibhaskar2@gmail.com");

        foreach($measurementTypes as $measurementType){
            $measure = app()->make(User::class)->addMeasurementType($user->id,$measurementType);
            
            if(!$measure){
                throw new Exception("Error while seeding user measurementType");
            }
        }
    }
}
