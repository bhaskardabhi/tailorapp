<?php

use Illuminate\Database\Seeder;
use App\Libraries\Repositories\Measurement;
use App\Libraries\Repositories\User;

class CustomerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $customers = [
            ['name' => 'Vinay Fakri','address' => 'Address 1','mobile' => '9145645945','email' => 'dabhibhaskar2@gmail.com'],
            ['name' => 'Rohan Patel','address' => 'Address 2','mobile' => '5645645645','email' => 'rohan@gmail.com'],
            ['name' => 'Vishal Ganatra','address' => 'Address 3','mobile' => '8967564565','email' => 'vishal@gmail.com'],
        ];

        $user = app()->make(User::class)->findByEmail("dabhibhaskar2@gmail.com");

        foreach($customers as $currentCustomer){
            $customer = app()->make(User::class)->addCustomer($user->id,$currentCustomer);
            
            if(!$customer){
                throw new Exception("Error while seeding user measurement");
            }
        }
    }
}
