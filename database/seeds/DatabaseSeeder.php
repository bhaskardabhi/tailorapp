<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::transaction(function ()
        {
            // $this->call(UserTableSeeder::class);
            // $this->call(MeasurementTypeTableSeeder::class);
            // $this->call(CustomerTableSeeder::class);
            $this->call(CustomerMeasurementSeeder::class);
        });
    }
}
